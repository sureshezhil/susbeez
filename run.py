
from flask import Flask
from flask_restful import Api
from flask_jwt_extended import JWTManager
app = Flask(__name__)
api = Api(app)
app.config['JWT_SECRET_KEY'] = 'jwt-secret-string'
jwt = JWTManager(app)

import views, models, resources

api.add_resource(resources.UserRegistration, '/registration')
# api.add_resource(resources.UserLogin, '/login')
# api.add_resource(resources.UserLogoutAccess, '/logout/access')
# api.add_resource(resources.UserLogoutRefresh, '/logout/refresh')
api.add_resource(resources.TokenRefresh, '/token/refresh')
# api.add_resource(resources.AllUsers, '/users')
# api.add_resource(resources.SecretResource, '/secret')

api.add_resource(resources.UserPosts, '/api/v1/UserPosts')
api.add_resource(resources.OnlineFriends, '/api/v1/OnlineFriends')
api.add_resource(resources.User, '/api/v1/User')
api.add_resource(resources.LikePost, '/api/v1/like')
