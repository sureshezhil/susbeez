from run import app
from flask import jsonify,render_template,request,session,redirect

from database import *
select=select()
insert=insert()
update=update()
delete=delete()
encrypt=encrypt()
app.secret_key = "suresh"

import datetime
import json
now = datetime.datetime.now()



@app.route("/")
def login():
	return render_template("login.html")

#Login validated Function
@app.route("/login_validate",methods=['POST','GET'])
def login_validate():
	user_id=request.form['user_id'] 
	password=request.form['password'] 
	if user_id=="" or password=="":
		return render_template("login.html",data="Please Enter the vaild User name or password")	
	else:
		password=encrypt.encrypt(password)
		data=select.login(user_id,password)
		if (data):
			session['id']=data[0]
			session['user_id']=data[1]
			session['user_name']=data[4]
			session['photo']=data[8]
			now = datetime.datetime.now()
			insert.session(data[0],data[4],data[8],now)
			return redirect("/dashboard")
		else:
			return render_template("login.html",data="Please Enter the vaild User name or password")


#SIGN UP PAGE
@app.route("/sign_up")
def sign_up():
	success=0
	return render_template("sign_up.html",success=success)

@app.route("/validate_signup",methods=['POST','GET'])
def validate():
	success=0
	user_id=request.form['user_id']
	mail_id=request.form['mail_id']
	if(mail_id=="" or user_id==""):
		return render_template("sign_up.html",success=success,data="enter both the UserId and email id")
	else:
		data=select.exit_users(user_id,mail_id)
		if(data):
			success=0
			return render_template("sign_up.html",success=success,data="userID or email id already exits")
		else:
			success=1
			return render_template("sign_up.html",success=success,user_id=user_id,mail_id=mail_id)

@app.route("/tem_signup",methods=['POST','GET'])
def tem_signup():
	user_id=request.form['user_id']
	mail_id=request.form['mail_id']
	user_name=request.form['user_name']
	dob=request.form['dob']
	gender=request.form['gender']
	phone=request.form['phone']
	files=request.files['file']
	password=request.form['password']
	repeat_pass=request.form['rep_pass']
	if(password== repeat_pass):
		password=encrypt.encrypt(password)
		if (files):
			filename = secure_filename(files.filename)
			filename1=filename
			files.save(os.path.join(app.config['Dp'], filename))
			file_extension=os.path.splitext("static/img/uploads/Dp/"+str(filename))[1]
			now = datetime.datetime.now()
			now=str(now)
			file="Media_Dp_"+str(user_id)+"_"+now[12:]+'_'+now[0:10]+file_extension
			os.rename("static/img/uploads/Dp/"+str(filename),"static/img/uploads/Dp/"+file)
		else:
			if(gender=="Male"):
				files=user_name[0].lower()
				file="alphabets/"+files+".jpg"
			else:
				files=user_name[0].lower()
				file="alphabets/"+files+"f.jpg"
		insert.signups(user_id,mail_id,user_name,password,phone,dob,gender,file)
		return redirect("/")
	else:
		success=1
		return render_template("sign_up.html",success=success,user_id=user_id,mail_id=mail_id,user_name=user_name,dob=dob,gender=gender,phone=phone,file=files)

@app.route("/dashboard")
def dashboard():
	return render_template("start.html",my_profile='s',userId=session['id'])
