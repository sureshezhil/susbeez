
from flask_restful import Resource, reqparse
from flask import jsonify
from database import *
select=select()
insert=insert()
update=update()
delete=delete()
encrypt=encrypt()
from flask_jwt_extended import (create_access_token, create_refresh_token, jwt_required, jwt_refresh_token_required, get_jwt_identity, get_raw_jwt)
class UserRegistration(Resource):
    def post(self):
        # parser = reqparse.RequestParser()
        # parser.add_argument('username', help = 'This field cannot be blank', required = True)
        # parser.add_argument('password', help = 'This field cannot be blank', required = True)
        # data = parser.parse_args()
        access_token = create_access_token(46)
        refresh_token = create_refresh_token(46)
        return {
                'message': 'User {} was created'.format(46),
                'access_token': access_token,
                'refresh_token': refresh_token
                }

class UserPosts(Resource):
    # @jwt_required
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('userId',required = True)
        params = parser.parse_args()
        access_token= create_access_token(identity = params.userId)
        return { 'data':select.select_list(params.userId),
                 'access_token' : access_token
                }

class OnlineFriends(Resource):
    # @jwt_required
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('userId',required = True)
        params = parser.parse_args()
        return jsonify(select.online_friends(params.userId))

class User(Resource):
    # @jwt_required
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument('userId',required = True)
        params = parser.parse_args()        
        return jsonify( select.user_profile(params.userId))

class LikePost(Resource):
    def post(self):
            parser = reqparse.RequestParser()
            parser.add_argument('userId',required = True)
            parser.add_argument('post_id',required = True)
            parser.add_argument('yourLike',required = True)
            params = parser.parse_args()   
            data=update.update_likes(params.post_id,params.userId,params.yourLike)
            return str(data[0])

class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        print current_user
        access_token = create_access_token(identity = current_user)
        return {'access_token': access_token}