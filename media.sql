-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 08, 2019 at 11:43 AM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `media`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_bp_friends`
--

CREATE TABLE `m_bp_friends` (
  `id` bigint(200) NOT NULL,
  `initiator_user_id` bigint(200) NOT NULL,
  `friend_user_id` bigint(200) NOT NULL,
  `is_confirmed` tinyint(1) DEFAULT '0',
  `is_limited` tinyint(1) DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_bp_friends`
--

INSERT INTO `m_bp_friends` (`id`, `initiator_user_id`, `friend_user_id`, `is_confirmed`, `is_limited`, `date_created`) VALUES
(40, 47, 46, 1, 0, '2018-12-30 17:56:56'),
(41, 48, 47, 1, 0, '2018-12-30 18:11:08'),
(42, 48, 46, 1, 0, '2018-12-30 22:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `m_bp_messages_messages`
--

CREATE TABLE `m_bp_messages_messages` (
  `id` bigint(20) NOT NULL,
  `thread_id` bigint(20) NOT NULL,
  `sender_id` bigint(20) NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_date` date DEFAULT NULL,
  `sent_time` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_bp_messages_messages`
--

INSERT INTO `m_bp_messages_messages` (`id`, `thread_id`, `sender_id`, `message`, `sent_date`, `sent_time`) VALUES
(24, 4647, 47, 'Hi', NULL, NULL),
(25, 4647, 46, 'hi da', NULL, NULL),
(26, 4647, 47, 'Good morning', NULL, NULL),
(27, 4647, 46, 'hi', NULL, NULL),
(28, 4647, 47, 'Good night', NULL, NULL),
(29, 4647, 46, 'hi', NULL, NULL),
(30, 4647, 46, '$scope.Coversations=response.data;         console.log(response);         console.log("mySuccess");', NULL, NULL),
(31, 4647, 46, 'hi', NULL, NULL),
(32, 4647, 46, 'hi', NULL, NULL),
(33, 4647, 47, 'Hhelloo', NULL, NULL),
(34, 4647, 47, 'Hi', NULL, NULL),
(35, 4647, 46, 'Good night', NULL, NULL),
(36, 4647, 46, 'ji', NULL, NULL),
(37, 4647, 47, 'Hi', NULL, NULL),
(38, 4647, 46, 'ji', NULL, NULL),
(39, 4647, 46, 'suresh', NULL, NULL),
(40, 4647, 46, 'hi', NULL, NULL),
(41, 4647, 47, 'Hi', NULL, NULL),
(42, 4647, 46, 'hi', NULL, NULL),
(43, 4647, 47, 'Hiffhafa', NULL, NULL),
(44, 4647, 47, 'Hi', NULL, NULL),
(45, 4647, 47, 'Hi', NULL, NULL),
(46, 4647, 46, ',lkjhgfdtfhjlk', NULL, NULL),
(47, 4647, 47, 'Hallo', NULL, NULL),
(48, 4647, 47, 'Ignition', NULL, NULL),
(49, 4647, 46, 'k', NULL, NULL),
(50, 4647, 46, 'kjhf', NULL, NULL),
(51, 4647, 47, 'Icing', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_bp_messages_recipients`
--

CREATE TABLE `m_bp_messages_recipients` (
  `id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `thread_id` bigint(20) NOT NULL,
  `unread_count` int(10) NOT NULL DEFAULT '0',
  `sender_only` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `m_comments`
--

CREATE TABLE `m_comments` (
  `id` bigint(200) NOT NULL,
  `post_id` bigint(200) NOT NULL,
  `user_id` bigint(200) NOT NULL,
  `comment` text NOT NULL,
  `comment_attachement` varchar(200) NOT NULL DEFAULT '0',
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `m_likes`
--

CREATE TABLE `m_likes` (
  `id` bigint(200) NOT NULL,
  `post_id` bigint(200) NOT NULL,
  `user_id` bigint(200) NOT NULL,
  `count` bigint(200) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_likes`
--

INSERT INTO `m_likes` (`id`, `post_id`, `user_id`, `count`) VALUES
(3, 220, 47, 1),
(4, 221, 47, 1),
(22, 225, 47, 1),
(38, 224, 47, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_post`
--

CREATE TABLE `m_post` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `post_content` varchar(2000) NOT NULL,
  `post_attachement` varchar(2000) NOT NULL DEFAULT '0',
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `post_likes` int(255) DEFAULT '0',
  `post_comments_count` bigint(200) NOT NULL DEFAULT '0',
  `bg_color` varchar(50) NOT NULL DEFAULT 'white'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_post`
--

INSERT INTO `m_post` (`id`, `user_id`, `post_content`, `post_attachement`, `time`, `post_likes`, `post_comments_count`, `bg_color`) VALUES
(199, 49, 'hi ..suresh', 'Media_activity_post_1234_8:41:44.063948_2018-12-31.jpg', '2018-12-31 18:41:44', 0, 0, 'orange'),
(220, 46, 'hi', 'Media_activity_post_suresh_1:05:35.265654_2019-01-03.jpg', '2019-01-03 21:05:35', 1, 0, 'aqua'),
(221, 46, 'Hi', 'Media_activity_post_suresh_1:15:13.892763_2019-01-03.jpg', '2019-01-03 21:15:13', 1, 0, 'aqua'),
(222, 46, 'hi', 'Media_activity_post_suresh_0:10:55.249914_2019-01-27.jpg', '2019-01-27 20:10:55', 0, 0, 'blue'),
(223, 46, 'mjj', 'Media_activity_post_suresh_0:28:17.383035_2019-01-27.jpg', '2019-01-27 20:28:17', 0, 0, '#f7f6f6'),
(224, 46, 'Hi da', 'Media_activity_post_suresh_0:32:15.854261_2019-02-03.jpg', '2019-02-03 20:32:15', 0, 0, '#000000'),
(225, 47, 'McIntyre', 'Media_activity_post_rajkumar_8:08:00.243430_2019-02-08.jpg', '2019-02-08 18:08:00', 0, 0, '#ffffff'),
(226, 46, 'jj', 'Media_activity_post_suresh_1:39:39.381293_2019-03-08.jpg', '2019-03-08 11:39:39', 0, 0, '#ffffff');

-- --------------------------------------------------------

--
-- Table structure for table `m_sessions`
--

CREATE TABLE `m_sessions` (
  `id` bigint(200) NOT NULL,
  `user_id` bigint(200) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `profile_pic` varchar(200) NOT NULL,
  `time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_sessions`
--

INSERT INTO `m_sessions` (`id`, `user_id`, `user_name`, `profile_pic`, `time`) VALUES
(91, 48, 'Vicky Vicky', 'Media_Dp_vicky_2:24:58.821005_2018-12-30.png', '15:29:09'),
(96, 46, 'SurEzh', 'Media_Dp_suresh_2:23:42.116538_2018-12-30.png', '17:36:58'),
(97, 47, 'Rajkumar', 'Media_Dp_rajkumar_2:24:20.850525_2018-12-30.png', '17:38:30');

-- --------------------------------------------------------

--
-- Table structure for table `m_signups`
--

CREATE TABLE `m_signups` (
  `signup_id` bigint(20) NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `registered` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `activation_key` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_signups`
--

INSERT INTO `m_signups` (`signup_id`, `user_login`, `user_pass`, `user_email`, `registered`, `activated`, `active`, `activation_key`) VALUES
(50, 'suresh', '}|o}r', 'suresh@gmail.com', '2018-12-30 22:23:42', '2018-12-30 22:23:42', 0, ''),
(51, 'rajkumar', '}|o}r', 'raj@gmail.com', '2018-12-30 22:24:20', '2018-12-30 22:24:20', 0, ''),
(52, 'vicky', '}|o}r', 'vicky@gmail.com', '2018-12-30 22:24:58', '2018-12-30 22:24:58', 0, ''),
(53, '1234', '}|o}r', '1234@gmail.com', '2018-12-31 18:37:49', '2018-12-31 18:37:49', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `m_users`
--

CREATE TABLE `m_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `phone` bigint(200) NOT NULL,
  `dob` date NOT NULL,
  `gender` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` text CHARACTER SET latin1 COLLATE latin1_general_ci,
  `cover_pic` text COLLATE utf8mb4_unicode_ci,
  `theme` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'skin-blue'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `m_users`
--

INSERT INTO `m_users` (`ID`, `user_login`, `user_pass`, `user_email`, `display_name`, `phone`, `dob`, `gender`, `photo`, `cover_pic`, `theme`) VALUES
(46, 'suresh', '}|o}r', 'suresh@gmail.com', 'SurEzh', 8940900902, '2018-12-06', 'Male', 'Media_Dp_suresh_2:23:42.116538_2018-12-30.png', NULL, 'skin-blue'),
(47, 'rajkumar', '}|o}r', 'raj@gmail.com', 'Rajkumar', 8940900999, '2018-12-25', 'Male', 'Media_Dp_rajkumar_2:24:20.850525_2018-12-30.png', NULL, 'skin-blue'),
(48, 'vicky', '}|o}r', 'vicky@gmail.com', 'Vicky Vicky', 99999999999999, '2018-11-29', 'Male', 'Media_Dp_vicky_2:24:58.821005_2018-12-30.png', NULL, 'skin-blue'),
(49, '1234', '}|o}r', '1234@gmail.com', '1234', 0, '2018-12-04', 'Male', 'Media_Dp_1234_8:37:49.050441_2018-12-31.png', NULL, 'skin-blue');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_bp_friends`
--
ALTER TABLE `m_bp_friends`
  ADD PRIMARY KEY (`id`),
  ADD KEY `initiator_user_id` (`initiator_user_id`),
  ADD KEY `friend_user_id` (`friend_user_id`);

--
-- Indexes for table `m_bp_messages_messages`
--
ALTER TABLE `m_bp_messages_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sender_id` (`sender_id`),
  ADD KEY `thread_id` (`thread_id`);

--
-- Indexes for table `m_bp_messages_recipients`
--
ALTER TABLE `m_bp_messages_recipients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `thread_id` (`thread_id`),
  ADD KEY `is_deleted` (`is_deleted`),
  ADD KEY `sender_only` (`sender_only`),
  ADD KEY `unread_count` (`unread_count`);

--
-- Indexes for table `m_comments`
--
ALTER TABLE `m_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_likes`
--
ALTER TABLE `m_likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_post`
--
ALTER TABLE `m_post`
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `m_sessions`
--
ALTER TABLE `m_sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_signups`
--
ALTER TABLE `m_signups`
  ADD PRIMARY KEY (`signup_id`),
  ADD KEY `activation_key` (`activation_key`),
  ADD KEY `user_email` (`user_email`),
  ADD KEY `user_login_email` (`user_login`,`user_email`);

--
-- Indexes for table `m_users`
--
ALTER TABLE `m_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_email` (`user_email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_bp_friends`
--
ALTER TABLE `m_bp_friends`
  MODIFY `id` bigint(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `m_bp_messages_messages`
--
ALTER TABLE `m_bp_messages_messages`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `m_bp_messages_recipients`
--
ALTER TABLE `m_bp_messages_recipients`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_comments`
--
ALTER TABLE `m_comments`
  MODIFY `id` bigint(200) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_likes`
--
ALTER TABLE `m_likes`
  MODIFY `id` bigint(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `m_post`
--
ALTER TABLE `m_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=227;
--
-- AUTO_INCREMENT for table `m_sessions`
--
ALTER TABLE `m_sessions`
  MODIFY `id` bigint(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `m_signups`
--
ALTER TABLE `m_signups`
  MODIFY `signup_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `m_users`
--
ALTER TABLE `m_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
